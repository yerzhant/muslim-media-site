import Vue from 'vue'
import App from './App'
import router from './router'
import {
  Vuetify,
  VApp,
  VNavigationDrawer,
  VList,
  VBtn,
  VIcon,
  VGrid,
  VToolbar,
  VCard,
  VSelect,
  VCarousel,
  VParallax,
  VTextField,
  VPagination,
  VDivider
  // transitions
} from 'vuetify'
import { Scroll } from 'vuetify/es5/directives'

import Axios from 'axios'
import VueYouTubeEmbed from 'vue-youtube-embed'
import VueCookies from 'vue-cookies'

import '../node_modules/vuetify/src/stylus/app.styl'
import 'mdi/css/materialdesignicons.css'
import './style/main.scss'

Vue.use(Vuetify, {
  components: {
    VApp,
    VNavigationDrawer,
    VList,
    VBtn,
    VIcon,
    VGrid,
    VToolbar,
    VCard,
    VSelect,
    VCarousel,
    VParallax,
    VTextField,
    VPagination,
    VDivider
    // transitions
  },
  directives: {
    Scroll
  },
  theme: {
    primary: '#b41a1f',
    secondary: '#424242',
    accent: '#82B1FF',
    error: '#FF5252',
    info: '#2196F3',
    success: '#4CAF50',
    warning: '#FFC107'
  }
})

Vue.use(VueYouTubeEmbed)
Vue.use(VueCookies)

Vue.prototype.$http = Axios.create({ baseURL: '/api' })
Vue.prototype.$isMobile = window.innerWidth <= 960

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
