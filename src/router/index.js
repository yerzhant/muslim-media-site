import Vue from 'vue'
import Router from 'vue-router'
import Home from '../components/Home'
import NotFound from '../components/NotFound'
import Events from '../components/Events'
import Quotations from '../components/Quotations'
import Library from '../components/Library'
import Madrasa from '../components/Madrasa'
import Posts from '../components/Posts'
import Post from '../components/Post'
import Search from '../components/Search'
import Teacher from '../components/Teacher'
import Tag from '../components/Tag'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    { path: '/', component: Home },
    { path: '/events', component: Events },
    { path: '/quotations', component: Quotations },
    { path: '/library/:section', component: Library, props: true },
    { path: '/madrasah/:section', component: Madrasa, props: true },
    { path: '/posts/:section', component: Posts, props: true },
    { path: '/post/:id', component: Post, props: true },
    { path: '/search', component: Search, props: true },
    { path: '/teacher/:id', component: Teacher, props: true },
    { path: '/tag/:tag', component: Tag, props: true },
    { path: '*', component: NotFound }
  ],
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  }
})
